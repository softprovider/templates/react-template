# TASKS

## General

-   All the tasks must be worked on this repository;
-   Additional packages must be asked permission first before installed and used;
-   The project structure defined in `README.md` must be respected;
-   The file naming and structure defined in `README.md` must be respected;
-   Always use Tailwind classes;
    -   If the value/color is repeated multiple times it should be added to the TailwindCSS theme;
-   The project must be done in TypeScript;
-   The design must be responsive (mobile screens, tablet screens, laptop screens and 4k screens)

## Specific

-   To add
